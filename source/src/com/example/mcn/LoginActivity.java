package com.example.mcn;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.mcn.AppSettings.API;
import com.example.utilities.PhoneUtil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class LoginActivity extends Activity {

	private Button btnLogin;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		btnLogin = (Button)findViewById(R.id.btnLogin);
		
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				Intent main = new Intent(LoginActivity.this, MainActivity.class);
//				startActivity(main);
				
				String deviceId = PhoneUtil.getDeviceId(getBaseContext(),
						getContentResolver());
				HttpRequestApi reAPI = new HttpRequestApi(
						getApplicationContext(), API.REG_DEVICE);
				reAPI.addParam("hid", deviceId);
				reAPI.addParam("type", "1");
				reAPI.addParam("username", "kingsley");
				reAPI.addParam("password", "mypassword");
				// Log.i("load API", list.size() + "ss1");
				reAPI.execute(new HttpRequestListener<JSONObject>() {
					// Hàm này sẽ thực thi trước
					@Override
					public void onPreExecute() {
						// TODO Auto-generated method stub
					}

					// Hàm này nhận ket qua trả zìa
					@Override
					public void onPostExecute(JSONObject result) {
						// TODO Auto-generated method stub
						try {
//							JSONArray jsonDeals = (JSONArray) result
//									.get("deals");
							String authKey = result.getString("auth_key");
							AppSettings.DEVICE_AUTH_KEY = authKey;
							Log.i("auth", authKey);
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						// Toast.makeText(getApplicationContext(),
						// ""+result.toString(),
						// Toast.LENGTH_LONG).show();
					}
				}); // END reAPI.execute
			}
		});
		
	}

	
}
