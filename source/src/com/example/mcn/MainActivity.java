package com.example.mcn;

import java.util.ArrayList;

import com.aphidmobile.flip.FlipViewController;
import com.example.mcn.adpater.MainActivityAdapter;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

public class MainActivity extends FragmentActivity implements
		OnItemClickListener {

	private SlidingMenu slidingMenu;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private ActionBar actionBar;
	private FlipViewController flipView;
	private ListView listMenu;
	private LinearLayout lnTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		flipView = new FlipViewController(this);

		// Use RGB_565 can reduce peak memory usage on large screen device, but
		// it's up to you to choose the best bitmap format
		flipView.setAnimationBitmapFormat(Bitmap.Config.RGB_565);

		flipView.setAdapter(new MainActivityAdapter(this));

		setContentView(flipView);

		// add left slide menu
		slidingMenu = new SlidingMenu(this);
		slidingMenu.setMode(SlidingMenu.LEFT);
		// nếu là TOUCHMODE_FULLSCREEN sẽ ko click dc trong menu
		slidingMenu.setTouchModeBehind(SlidingMenu.TOUCHMODE_MARGIN);
		// slidingMenu.setShadowDrawable(R.drawable.actionbar_gradient);
		slidingMenu.setShadowWidth(30);
		slidingMenu.setFadeDegree(0.0f);
		slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

		slidingMenu.setBehindWidth(650);
		slidingMenu.setMenu(R.layout.sliding_menu);

		lnTitle = (LinearLayout) findViewById(R.id.lnTitle);

		actionBar = getActionBar();
		// actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setIcon(R.drawable.icon);
		actionBar.setHomeButtonEnabled(true);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.slidingMenu);
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description */
		R.string.drawer_close /* "close drawer" description */
		) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				// getActionBar().setTitle(mTitle);
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				// getActionBar().setTitle(mDrawerTitle);
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// adapter sliding menu
		listMenu = (ListView) findViewById(R.id.listMenu);
		listMenu.setAdapter(new adapter(this));
		listMenu.setOnItemClickListener(this);

		lnTitle.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent userInfo = new Intent(MainActivity.this,
						UserInfoActivity.class);
				startActivity(userInfo);

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		// Init search manager
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// onBackPressed();
			slidingMenu.toggle();
			return true;
		case R.id.action_search:
			
			return true;
		case R.id.action_refresh:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onResume() {
		super.onResume();
		flipView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		flipView.onPause();
	}

	// sliding menu

	// list menu
	class singleRow {
		String menu_name;
		int image;

		public singleRow(String menu_name, int image) {
			// TODO Auto-generated constructor stub
			this.menu_name = menu_name;
			this.image = image;

		}

	}

	class adapter extends BaseAdapter {

		ArrayList<singleRow> list;
		Context context;

		public adapter(Context c) {
			context = c;
			// TODO Auto-generated constructor stub
			list = new ArrayList<singleRow>();

			Resources res = c.getResources();
			String[] menu_name = res.getStringArray(R.array.menu_name);

			int[] images = { R.drawable.icon_kiemtrathe,
					R.drawable.icon_gantoi, R.drawable.icon_barcode,
					R.drawable.icon_setting, R.drawable.icon_thongtin };
			for (int i = 0; i < 5; i++) {
				singleRow s = new singleRow(menu_name[i], images[i]);
				list.add(s);
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();

		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return list.get(position);

		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;

		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub

			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View row = inflater.inflate(R.layout.single_row_menu, listMenu,
					false);

			TextView menu_name = (TextView) row.findViewById(R.id.tvMenu);
			ImageView image = (ImageView) row.findViewById(R.id.imMenu);

			singleRow temp = list.get(position);
			menu_name.setText(temp.menu_name);
			image.setImageResource(temp.image);
			return row;

		}

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		// Toast.makeText(getApplicationContext(), "You clicked on position : "
		// + position + " and id : " + id, Toast.LENGTH_LONG).show();
		switch (position) {
		case 0:
			// Intent home = new Intent("com.jvit.findvoucher.MAINACTIVITY");
			// startActivity(home);
			// break;
		case 1:

			break;
		case 2:
			Intent barcodeScreen = new Intent(this, BarcodeActivity.class);
			startActivity(barcodeScreen);
			break;
		case 3:

			break;
		case 4:

			break;
		case 5:
			// share app

			break;

		case 6:

			break;
		default:
			break;
		}

	}

}
