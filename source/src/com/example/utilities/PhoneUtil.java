package com.example.utilities;

import java.util.UUID;

import android.content.ContentResolver;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhoneUtil {

	/*
	 * get DeviceId
	 * Params from fragment, activity: getBaseContext(), getContentResolver() 
	 */
	public static String getDeviceId(Context context, ContentResolver contentResolver){
    	final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

        final String tmDevice, tmSerial, androidId;
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + android.provider.Settings.Secure.getString(contentResolver, android.provider.Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        Log.i("device-id", "tmDevice" + tmDevice);
        Log.i("device-id", deviceId);
        Log.i("device-id", "tmSerial: " + tmSerial);
        Log.i("device-id", "androidId: " + androidId);
        return deviceId;
    }
	
	public static String osModel(){
		return android.os.Build.MODEL;
	}
}
