package com.example.mcn.adpater;

import java.util.ArrayList;
import java.util.List;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.sax.StartElementListener;
import android.text.Html;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aphidmobile.utils.AphidLog;
import com.aphidmobile.utils.IO;
import com.aphidmobile.utils.UI;
import com.example.mcn.ListByCate;
import com.example.mcn.R;
import com.example.mcn.data.CategoryMainData;

public class MainActivityAdapter extends BaseAdapter {

	Context context_;
	 private LayoutInflater inflater;

	  private int repeatCount = 1;

	  private List<CategoryMainData.Data> categoryData;
	  
	  public MainActivityAdapter(Context context) {
		    inflater = LayoutInflater.from(context);
		    categoryData = new ArrayList<CategoryMainData.Data>(CategoryMainData.IMG_DESCRIPTIONS);
		    context_ = context;
		  }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return categoryData.size() * repeatCount;
	}

	 public void setRepeatCount(int repeatCount) {
		    this.repeatCount = repeatCount;
		  }
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 View layout = convertView;
		    if (convertView == null) {
		      layout = inflater.inflate(R.layout.complex1, null);
		      AphidLog.d("created new view from adapter: %d", position);
		    }

		    final CategoryMainData.Data data = categoryData.get(position % categoryData.size());

//		    UI
//		        .<TextView>findViewById(layout, R.id.title)
//		        .setText(AphidLog.format("%d. %s", position, data.title));
//
//		    UI
//		        .<ImageView>findViewById(layout, R.id.photo)
//		        .setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data.imageFilename));
//
//		    UI
//		        .<TextView>findViewById(layout, R.id.description)
//		        .setText(Html.fromHtml(data.description));
//
//		    UI
//		        .<Button>findViewById(layout, R.id.wikipedia)
//		        .setOnClickListener(new View.OnClickListener() {
//		          @Override
//		          public void onClick(View v) {
//		            Intent intent = new Intent(
//		                Intent.ACTION_VIEW,
//		                Uri.parse(data.link)
//		            );
//		            inflater.getContext().startActivity(intent);
//		          }
//		        });
		    
		    UI
		    .<RelativeLayout>findViewById(layout, R.id.topleft)
		    .setOnDragListener(new MyDragListener());
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage1)
		    .setOnTouchListener(new MyTouchListener());
		    
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage1)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage2)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage3)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage4)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage5)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});
		    UI
		    .<ImageView>findViewById(layout, R.id.myimage6)
		    .setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent detailCate = new Intent(context_, ListByCate.class);
					context_.startActivity(detailCate);
				}
			});

		   
		    return layout;
	}

	public void removeData(int index) {
	    if (categoryData.size() > 1) {
	    	categoryData.remove(index);
	    }
	  }
	
	
	 private final class MyTouchListener implements OnTouchListener {
		    public boolean onTouch(View view, MotionEvent motionEvent) {
		      if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
		        ClipData data = ClipData.newPlainText("", "");
		        DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
		        view.startDrag(data, shadowBuilder, view, 0);
		        view.setVisibility(View.INVISIBLE);
		        return true;
		      } else {
		        return false;
		      }
		    }
		  }
	  
	  class MyDragListener implements OnDragListener {
		    Drawable enterShape = context_.getResources().getDrawable(R.drawable.shape_droptarget);
		    Drawable normalShape = context_.getResources().getDrawable(R.drawable.shape);

		  

			@Override
			public boolean onDrag(View v, DragEvent event) {
				// TODO Auto-generated method stub
				 int action = event.getAction();
			      switch (event.getAction()) {
			      case DragEvent.ACTION_DRAG_STARTED:
			        // do nothing
			        break;
			      case DragEvent.ACTION_DRAG_ENTERED:
			        v.setBackgroundDrawable(enterShape);
			        break;
			      case DragEvent.ACTION_DRAG_EXITED:
			        v.setBackgroundDrawable(normalShape);
			        break;
			      case DragEvent.ACTION_DROP:
			        // Dropped, reassign View to ViewGroup
			        View view = (View) event.getLocalState();
			        ViewGroup owner = (ViewGroup) view.getParent();
			        owner.removeView(view);
			        RelativeLayout container = (RelativeLayout) v;
			        container.addView(view);
			        view.setVisibility(View.VISIBLE);
			        break;
			      case DragEvent.ACTION_DRAG_ENDED:
			        v.setBackgroundDrawable(normalShape);
			      default:
			        break;
			      }
			      return true;
			}
		  }
	
	
	
}
