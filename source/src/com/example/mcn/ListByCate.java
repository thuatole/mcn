package com.example.mcn;

import com.aphidmobile.flip.FlipViewController;
import com.example.mcn.adpater.ListByCateAdapter;



import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;


public class ListByCate extends Activity {

	private ActionBar actionBar;
	private FlipViewController flipView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		 flipView = new FlipViewController(this);

	        //Use RGB_565 can reduce peak memory usage on large screen device, but it's up to you to choose the best bitmap format 
	        flipView.setAnimationBitmapFormat(Bitmap.Config.RGB_565);

	        flipView.setAdapter(new ListByCateAdapter(this));

	        setContentView(flipView);
	        
	        actionBar = getActionBar();
			// actionBar.setDisplayShowCustomEnabled(true);
			actionBar.setDisplayHomeAsUpEnabled(true);
			actionBar.setDisplayShowCustomEnabled(true);
			actionBar.setIcon(R.drawable.icon);
			actionBar.setHomeButtonEnabled(true);
	}
	
	 @Override
	  protected void onResume() {
	    super.onResume();
	    flipView.onResume();
	  }

	  @Override
	  protected void onPause() {
	    super.onPause();
	    flipView.onPause();
	  }
	  
	  @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle action bar item clicks here. The action bar will
	        // automatically handle clicks on the Home/Up button, so long
	        // as you specify a parent activity in AndroidManifest.xml.
//	        int id = item.getItemId();
//	        if (id == R.id.action_settings) {
//	            return true;
//	        }
	    	switch (item.getItemId()) {
	    	case android.R.id.home:
				// onBackPressed();
				
				
				onBackPressed();
				return true;
			default:
						
	        return super.onOptionsItemSelected(item);
	    	}
	    }
	
}
