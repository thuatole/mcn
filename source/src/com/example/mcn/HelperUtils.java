package com.example.mcn;

import com.example.mcn.AppSettings.API;

public class HelperUtils {

	/**
	 * Build api uri
	 * 
	 * @param enumAPI
	 * @return
	 */
	public static String getAPIUri(API enumAPI) {
		String uri = null;

		switch (enumAPI) {
		case DATA_NOTICES:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_NOTICES;
			break;
		case REG_DEVICE:
			uri = AppSettings.API_SERVER + AppSettings.API_REG_DEVICE;
			break;
		case DATA_DEAL:
			uri = AppSettings.API_SERVER + AppSettings.API_DATA_DEAL;
			break;
		default:
			break;
		}
		return uri;
	}

}
