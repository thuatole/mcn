package com.example.mcn;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.mcn.AppSettings.API;

public class HttpRequestApi {

	/**
	 * Params
	 */
	List<NameValuePair> params;

	/**
	 * Context
	 */
	Context mContext;

	/**
	 * URI
	 */
	String uri;

	public HttpRequestApi(Context context, API enumAPI) {
		// TODO Auto-generated constructor stub

		this.params = new ArrayList<NameValuePair>();
		this.mContext = context;

		this.uri = HelperUtils.getAPIUri(enumAPI);
		
		Log.i("test", this.uri);
	}

	/**
	 * Adds the param.
	 * 
	 * @param name
	 * @param value
	 */
	public void addParam(String name, String value) {
		params.add(new BasicNameValuePair(name, value));
	}

	/**
	 * Execute async
	 */
	public void execute(
			final HttpRequestListener<JSONObject> pHttpRequestListener) {
		final AsyncTask<Void, Void, JSONObject> async = new AsyncTask<Void, Void, JSONObject>() {

			@Override
			protected void onPreExecute() {
				// TODO Auto-generated method stub
				super.onPreExecute();

				pHttpRequestListener.onPreExecute();
			}

			@Override
			protected JSONObject doInBackground(Void... params) {
				// TODO Auto-generated method stub
				return HttpRequestApi.this.doRequest();
			}

			@Override
			protected void onPostExecute(JSONObject result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				// if (pHttpRequestListener != null && result != null) {
				pHttpRequestListener.onPostExecute(result);
				// }

			}
		};

		async.execute();
	}

	/**
	 * execute request to server
	 * 
	 * @return
	 */
	public JSONObject doRequest() {
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(uri);

		if (!params.isEmpty()) {
			try {
				Log.i("BASEMAP", params.toString());
				httppost.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		try {
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			if (entity != null) {
				return new JSONObject(EntityUtils.toString(entity));
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
