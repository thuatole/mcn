package com.example.mcn.adpater;

import java.util.ArrayList;
import java.util.List;

import com.aphidmobile.utils.AphidLog;
import com.aphidmobile.utils.UI;
import com.example.mcn.ListByCate;
import com.example.mcn.R;
import com.example.mcn.data.CategoryMainData;
import com.example.mcn.data.ListByCateData;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class ListByCateAdapter extends BaseAdapter {
	Context context_;
	 private LayoutInflater inflater;

	  private int repeatCount = 1;

	  private List<ListByCateData.Data> listyData;
	  
	  public ListByCateAdapter(Context context) {
		    inflater = LayoutInflater.from(context);
		    listyData = new ArrayList<ListByCateData.Data>(ListByCateData.IMG_DESCRIPTIONS);
		    context_ = context;
		  }
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listyData.size() * repeatCount;
	}

	 public void setRepeatCount(int repeatCount) {
		    this.repeatCount = repeatCount;
		  }
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		 View layout = convertView;
		    if (convertView == null) {
		      layout = inflater.inflate(R.layout.listbycate, null);
		      AphidLog.d("created new view from adapter: %d", position);
		    }

		    final ListByCateData.Data data = listyData.get(position % listyData.size());

//		    UI
//		        .<TextView>findViewById(layout, R.id.title)
//		        .setText(AphidLog.format("%d. %s", position, data.title));
//
//		    UI
//		        .<ImageView>findViewById(layout, R.id.photo)
//		        .setImageBitmap(IO.readBitmap(inflater.getContext().getAssets(), data.imageFilename));
//
//		    UI
//		        .<TextView>findViewById(layout, R.id.description)
//		        .setText(Html.fromHtml(data.description));
//
//		    UI
//		        .<Button>findViewById(layout, R.id.wikipedia)
//		        .setOnClickListener(new View.OnClickListener() {
//		          @Override
//		          public void onClick(View v) {
//		            Intent intent = new Intent(
//		                Intent.ACTION_VIEW,
//		                Uri.parse(data.link)
//		            );
//		            inflater.getContext().startActivity(intent);
//		          }
//		        });
		    
//		    UI
//		    .<RelativeLayout>findViewById(layout, R.id.topleft)
//		    .setOnDragListener(new MyDragListener());
//		    UI
//		    .<ImageView>findViewById(layout, R.id.myimage1)
//		    .setOnTouchListener(new MyTouchListener());
		    
		   
		   
		    return layout;
	}

	public void removeData(int index) {
	    if (listyData.size() > 1) {
	    	listyData.remove(index);
	    }
	  }
	
	
	
	 
	
}
