package com.example.mcn;

public interface HttpRequestListener<T> {

	public void onPostExecute(T result);

	public void onPreExecute();
}